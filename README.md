# Installation de l'environnement (on verra peut etre ça ensemble)
- installe pip
- installe le programme et la bibliothèque search-tweets-python pour récupérer des tweets : `pip install searchweets-v2`
- créé un fichier ".twitter_keys.yaml" dans ton répertoire personnel contenant :
```
search_tweets_v2:
  endpoint:  https://api.twitter.com/2/tweets/search/recent
  consumer_key: <CONSUMER_KEY>
  consumer_secret: <CONSUMER_SECRET>
  bearer_token: <BEARER_TOKEN>

```
en remplacant les `<CLE>` convenablement

# Restrictions (tu peux sauter ce paragraphe dans un premier temps)
voir https://developer.twitter.com/en/portal/dashboard et https://developer.twitter.com/en/docs/twitter-api/rate-limits pour les quotas

# Prise en main
### Requêtes
tu peux maintenant utiliser la commande `search_tweets.py`. `search_tweets.py -h` pour obtenir l'aide de la commande.
exemple simple :

`search_tweets.py --max-tweets 20 --query 'lang:fr (covid OR covid19)'`
ça retourne au maximum les 20 derniers tweets dans les 7 derniers jours (accès standard) classés par twitter comme étant en français et contenant covid ou covid19

`search_tweets.py --max-tweets 20 --query 'lang:fr (covid OR covid19) #sterile'`
Meme chose que précédement mais sur les tweets qui contiennent aussi le hashtag sterile, donc a priori cette deuxième commande retourne un sous ensemble de la première.

L'ensemble des opérateurs se trouve [la](https://developer.twitter.com/en/docs/twitter-api/tweets/search/integrate/build-a-query)

Par exemple, tu peux ne pas vouloir récupérer les retweets. Dans ce cas tu utilises l'opérateur de négation `-` sur l'opérateur `is:retweet` => `-is:retweet`. Comme tu l'as constaté, la requète se trouve entre guillemets simples après l'option -query du programme search_tweets.py. L'opérateur AND ne doit pas être écrit contrairement à l'opérateur OR. C'est simplement un espace entre deux termes.

Ainsi, quel est la requète permettant de trouver tous les tweets (sans les retweets) qui contiennent le hashtag 5g ET (le hashtag covid OU le hashtag coronavirus OU le hashtag covid19) ET classé par twitter comme étant en francais?

.

.

.

`lang:fr -is:retweet #5g (covid OR covid19 OR coronavirus)`

Note que ton quota diminue https://developer.twitter.com/en/portal/dashboard et qu'il est plus élevé en "Academic Research". Il y'a d'autres limitations, moins restrictives avec AR. Par exemple en standard, la requete à une taille max de 512 caractères, 1024 en AR. Tu as plus d'opérateurs en AR. Tu peux récupérer plus de tweets en AR, etc.

GO AR GIRL!

### Champs (fields)
Par défaut la commande te retourne 2 champs pour chaque tweet. 
- "id" : l'identifiant unique du tweet, un nombre.
- "text" : le texte du tweet.

Il y a des tas d'autres champs : https://developer.twitter.com/en/docs/twitter-api/data-dictionary/object-model/tweet

L'option pour spécifier quels champs tu veux c'est --tweet-fields. Par exemple : 
`search_tweets.py --max-tweets 10 --query 'lang:fr (covid OR covid19)' --tweet-fields 'created_at,public_metrics'` si en plus d'id et text tu veux la date et l'heure du tweet et les "public metrics". Les "Public Metrics" c'est intéressant : nombre de retweet, nombre de réponses, nombre de likes, nombre de quotes. Je crois que tu as accès à plus de metrics avec un AR.

### Expansions
`search_tweets.py --max-tweets 10 --query 'lang:fr (covid OR covid19)' --tweet-fields 'created_at,public_metrics' --expansions 'author_id' --atomic` Pour avoir en plus l'auteur

# Quand t'as fini de jouer
installe jupyter : `pip install jupyter`, récupère twitter_covid_conspiracy.ipynb dans un dossier et exécute `jupyter notebook` dans ce dossier
